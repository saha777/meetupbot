package com.meetup.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class HelloRestApi {
    private final static String HELLO_REST_API_URL = "http://localhost:8089/api/hello";

    private final RestTemplate restTemplate;

    public HelloRestApi() {
        restTemplate = new RestTemplate();
    }

    public String getHello() {
        ResponseEntity<String> response =
                restTemplate.getForEntity(HELLO_REST_API_URL, String.class);
        return response.getBody();
    }
}
