package com.meetup;

import com.meetup.rest.HelloRestApi;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import java.util.List;
//import org.apache.log4j.*;

public class TelegramBot extends TelegramLongPollingBot {
    private final HelloRestApi helloRestApi;

    public TelegramBot() {
        this.helloRestApi = new HelloRestApi();
    }

    //example answer TEXT
    public void sendTextMsg(Message msg, String text) {
        try {
            execute(new SendMessage().setChatId(msg.getChatId()).setText(text));
        } catch (Exception e) {
//            LOG.error("Can't send Text message: ", e);
        }
    }

    //example answer GIF
    public void sendGIFMsg(Message msg, String gif) {
        try {
            execute(new SendDocument().setChatId(msg.getChatId()).setDocument(gif));
        } catch (Exception e) {
//            LOG.error("Can't send GIF message: ", e);
        }
    }

    //example answer IMAGE
    public void sendImageMsg(Message msg, String image) {
        try {
            execute(new SendPhoto().setChatId(msg.getChatId()).setPhoto(image));
        } catch (Exception e) {
//            LOG.error("Can't send Photo message: ", e);
        }
    }
    @Override
    public void onUpdateReceived(Update update) {
        sendTextMsg(update.getMessage(), helloRestApi.getHello());

    }
    @Override
    public void onUpdatesReceived(List<Update> updates) {
        for (Update u : updates){
            sendTextMsg(u.getMessage(), helloRestApi.getHello());
        }
    }

    @Override
    public String getBotUsername() {
        return "ComponentsPILabaBot";
    }

    @Override
    public String getBotToken() {
        return "641862162:AAG2z77r1l4xRguRn1RGJecIOEHdSjthHAk";
    }

    //example Start
    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi botapi = new TelegramBotsApi();

        try {
            botapi.registerBot(new TelegramBot());
        } catch (TelegramApiException e) {
            //write error massage
        }
    }
}